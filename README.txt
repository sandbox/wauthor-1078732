Description
The Q-ImageUploader module integrates the commercial 'Q-ImageUploader Pro'
(http://quadroland.com/q_imageuploader/) with Drupal.
 Multiple images are uploaded in a single batch into a image module gallery.

Requirements
 - Modules: image, imagecache, swfobject_api 
 - 'Q-ImageUploader Pro' is a commercial product. A valid license key is required.
 
